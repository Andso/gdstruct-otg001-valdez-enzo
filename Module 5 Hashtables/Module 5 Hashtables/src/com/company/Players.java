package com.company;

import java.util.Objects;

public class Players {

    private int playerId;
    private String username;
    private int level;


    public Players(int playerId, String username, int level) {
        this.playerId = playerId;
        this.username = username;
        this.level = level;
    }


    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Players players = (Players) o;
        return playerId == players.playerId && level == players.level && Objects.equals(username, players.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerId, username, level);
    }


    @Override
    public String toString() {
        return "Players{" +
                "playerId=" + playerId +
                ", username='" + username + '\'' +
                ", level=" + level +
                '}';
    }



}
