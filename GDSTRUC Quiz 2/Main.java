package com.company;


public class Main {

    public static void main(String[] args) {

    Player Kirito = new Player(1, "Kirito", 140);
    Player Asuna = new Player(2, "Asuna", 130);
    Player Sicilia = new Player ( 3, "Sicilia" , 95);
    Player Klein = new Player ( 4, "Klein" , 115);
    Player Elizabeth = new Player ( 4, "Elizabeth" , 104);
    Player Leafa = new Player ( 5, "Leafa" , 112);
    Player Sinon = new Player ( 5, "Sinon" , 89);

    PlayerLinkedList playerLinkedList = new PlayerLinkedList();
    playerLinkedList.addToFront(Kirito);
    playerLinkedList.addToFront(Asuna);
    playerLinkedList.addToFront(Sicilia);
    playerLinkedList.addToFront(Klein);
    playerLinkedList.addToFront(Elizabeth);
    playerLinkedList.addToFront(Leafa);
    playerLinkedList.addToFront(Sinon);

    playerLinkedList.printList();
    playerLinkedList.listSize();
    playerLinkedList.mixedList();

    }
}
