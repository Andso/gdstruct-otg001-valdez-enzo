package com.company;

public class PlayerLinkedList {
    private PlayerNode head;


    public void addToFront(Player player)
    {
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;
    }
    public void removeFirst()
    {
        PlayerNode current = head;
        System.out.print("HEAD -> ");

        while(current != null){

            System.out.print(current);
            System.out.print(" -> ");
            current = current.getNextPlayer();
        }


        System.out.println("null");

    }

    public void listSize()
    {
        PlayerNode current = head;
        int size = 0;
        while(current != null)
        {
            size++;
            current = current.getNextPlayer();

        }
        System.out.print("Size of List : ");
        System.out.print(size);
        System.out.print("   ");
    }

    public void printList()
    {

        PlayerNode current = head;
        System.out.print("HEAD -> ");
        while(current != null){
                System.out.print(current);
                System.out.print(" -> ");
                current = current.getNextPlayer();
        }

        System.out.println("null");
    }

    public void mixedList()
    {

        PlayerNode current = head;

        while(current != null){

            if(head == current.getNextPlayer())
            {
                System.out.println(" True ");
            }
            else
            {
                System.out.println(" False ");
            }

            if(head == current.getNextPlayer())
            {
                System.out.println(" 1 ");
            }
            else
            {
                System.out.println(" -1 ");
            }
            current = current.getNextPlayer();
        }

    }
}
