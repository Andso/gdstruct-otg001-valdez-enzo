package com.company;


import java.util.NoSuchElementException;
import java.util.Objects;

public class Matchmaking {


    private Players[] queue;
    private int front;
    private int back;

    public Matchmaking (int capacity)
    {
        queue = new Players[capacity];
    }

    public void add(Players player)
    {

        // Resizes array when full

        if(back == queue.length)
        {

            Players[] newArray = new Players[queue.length * 2];
            System.arraycopy(queue, 0, newArray, 0, queue.length);
            queue = newArray;
        }

        queue[back] = player;
        back++;



    }


    public Players remove()
    {
        if (size() == 0) {
            throw new NoSuchElementException();
        }

        Players removedPlayer = queue[front];
        queue[front] = null;
        front++;

        if (size() == 0)
        {
            front = 0;
            back = 0;
        }

        return  removedPlayer;

    }


    public Players peek()
    {

    if(size() == 0)
    {
        throw  new NoSuchElementException();
    }

    return  queue[front];
    }


    public int size()
    {
     return back - front;
    }


    public  void printQueue()
    {
        for (int i = front; i < back; i++)
        {
            System.out.println(queue[i]);
        }


    }




}
