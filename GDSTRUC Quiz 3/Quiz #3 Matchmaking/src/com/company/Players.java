package com.company;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Objects;

public class Players {


    private int id;
    private String playerName;

    public Players(int id, String playerName) {
        this.id = id;
        this.playerName = playerName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Players players = (Players) o;
        return id == players.id &&
                Objects.equals(playerName, players.playerName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, playerName);
    }


    @Override
    public String toString() {
        return "Players{" +
                "id=" + id +
                ", playerName='" + playerName + '\'' +
                '}';
    }
}
