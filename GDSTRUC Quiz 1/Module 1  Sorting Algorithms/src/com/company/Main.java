package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here



        int[] numbers = new int[10];

        numbers[0]= 20;
        numbers[1]= 12;
        numbers[2]= 6;
        numbers[3]= 26;
        numbers[4]= 67;
        numbers[5]= 47;
        numbers[6]= 69;
        numbers[7]= 16;
        numbers[8]= 34;
        numbers[9]= 5;


       // System.out.println("Before Bubble Sort");
        //printArrayElements(numbers);

        //bubbleSort(numbers);

        //System.out.println("\n\nAfter Bubble Sort");
       // printArrayElements(numbers);

    System.out.println("Before selection Sort");
    printArrayElements(numbers);

    selectionSort(numbers);

    System.out.println("\n\nAfter selection Sort");
    printArrayElements(numbers);

    }

    private static void bubbleSort(int[] arr)
    {
        for (int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            for(int i = 0; i < lastSortedIndex; i++)
            {
                if (arr[i] < arr[i+1])
                {
                    int temp = arr[i];
                    arr[i] = arr[i+1];
                    arr[i+1] = temp;

                }
            }
        }
    }



    private static void printArrayElements(int[] arr)
    {
        for (int j : arr) {
            System.out.println(j + " ");
        }
    }


    private static void selectionSort(int[] arr)
    {
        for(int lastSortedIndex = arr.length - 1 ; lastSortedIndex > 0; lastSortedIndex--)
        {
            int largestIndex = 0;

            for (int i = 1; i <= lastSortedIndex; i++)
            {
                if(arr[i] < arr[largestIndex])
                {
                    largestIndex = i;
                }

            }
            int temp = arr[lastSortedIndex];
            arr[lastSortedIndex] = arr[largestIndex];
            arr[largestIndex] = temp;

        }



    }



}
